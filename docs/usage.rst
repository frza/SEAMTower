==========================
The SEAMTower Design Model
==========================

The SEAMTower design model uses a simple approach to size the tower wall thickness, using only the tower bottom extreme load and tower bottom lifetime equivalent load....ADD more text here.


Example
-------

An example of how to use the model can be found in ``src/SEAMTower/examples/simple.py``.
We create a basic assembly where we add the tower model, and set the model's inputs.
Note that in this example we have set the extreme and equivalent loads manually, since we're running the model stand alone.
When run together with a rotor loads model, the tower model would receive its inputs from the loads model.

The inputs are based on a turbine with a rated power of 3 MW, rotor diameter of 110 m and a hub height of 110 m.
The resulting mass of the tower is approximately 337 tonnes, with a wall thickness distribution plotted below.

.. literalinclude:: ../src/SEAMTower/examples/simple.py


As shown in the plot the tower thickness is designed by fatigue loads on the lower half and the extreme load on the upper half.


.. _simple_thickness_fig:


    .. image::  figures/simple_thickness.*
       :width: 49 %
       :align: center



Consult the :ref:`SEAMTower_src_label` section for more detail.
