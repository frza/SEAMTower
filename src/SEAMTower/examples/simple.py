
import numpy as np
from openmdao.main.api import Assembly

from SEAMTower.SEAMTower import SEAMTower

class TowerAssembly(Assembly):

    def configure(self):

        self.add('tower', SEAMTower(21))
        self.driver.workflow.add('tower')

        self.tower.hub_height = 100.
        self.tower.D_bottom = 4.
        self.tower.D_top = 2.

        self.tower.WohlerExpTower = 4.
        self.tower.Slim_ext = 235.0
        self.tower.Slim_fat = 14.885
        self.tower.SF_tower = 1.5
        self.tower.Neq = 1.e7

        # set extreme loads (which will typically be calculated by a loads model)
        self.tower.M_bottom = 8.31829156476416E+004
        self.tower.M_leq = 3.4090546856856967E+0004


if __name__ == '__main__':

    top = TowerAssembly()
    top.run()
    print 'Tower mass: %f' % top.tower.mass

    import matplotlib.pylab as plt

    plt.plot(top.tower.height, top.tower.tfat, 'b-<', label='Fatigue based')
    plt.plot(top.tower.height, top.tower.text, 'g->', label='Extreme load based')
    plt.plot(top.tower.height, top.tower.t, 'r-o', label='Final')
    plt.xlabel('Height [m]')
    plt.ylabel('Wall thickness [m]')
    plt.legend(loc='best')
    plt.show()
