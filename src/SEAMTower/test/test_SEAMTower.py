
import numpy as np
import unittest

from openmdao.main.api import Assembly

from SEAMTower.SEAMTower import SEAMTower

# data from Kenneth's model
t_org = np.array([ 0.045     ,  0.0454    ,  0.0458    ,  0.0462    ,  0.0466    ,
        0.0469    ,  0.0471    ,  0.0473    ,  0.0474    ,  0.0474    ,
        0.0473    ,  0.04722722,  0.04742716,  0.0475277 ,  0.04750369,
        0.04732368,  0.04694809,  0.04632685,  0.04539609,  0.04407372,
        0.04225329])

mass = 337068.09

class TowerAssembly(Assembly):

    def configure(self):

        self.add('tower', SEAMTower(21))
        self.driver.workflow.add('tower')

        self.tower.hub_height = 100.
        self.tower.tower_bottom_diameter = 4.
        self.tower.tower_top_diameter = 2.
        self.tower.tower_bottom_moment_max = 8.31829156476416E+004
        self.tower.tower_bottom_moment_leq = 3.4090546856856967E+0004
        self.tower.wohler_exponent_tower = 4.
        self.tower.stress_limit_extreme_tower = 235.0
        self.tower.stress_limit_fatigue_tower = 14.885
        self.tower.safety_factor_tower = 1.5
        self.tower.lifetime_cycles = 1.e7

class SEAMTowerTestCase(unittest.TestCase):

    def setUp(self):
        self.top = TowerAssembly()
        self.top.run()

    def test_SEAMTower(self):

        self.assertAlmostEqual(self.top.tower.tower_mass, 336873.41925864294, places=8)

if __name__ == "__main__":
    unittest.main()
    # top = TowerAssembly()
    # top.run()
    # import matplotlib.pylab as plt
    # plt.plot(top.tower.height, t_org, '-o', label='Original model')
    # plt.plot(top.tower.height, top.tower.t, '->', label='SEAMTower')
    # plt.legend(loc='best')
    # plt.show()
